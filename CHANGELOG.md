# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.1.2] - 2017-02-19
### Changed
- Remove unnecessary console log

## [1.1.1] - 2017-02-19
### Changed
- Set encoding and description on index

## [1.1.0] - 2017-02-19
### Added
- Add resizing based on scrollwheel
- Make mouse tracking smoother
- Make colors transition smoothly
- Hide cursor when not moving, crosshair when moving

## [1.0.1] - 2017-02-16
### Added
- Add mouse following
- Fix README links

## 1.0.0 - 2016-11-10
### Added
- Basic configuration for jsdoc
- Basic configuration for eslint
- Basic configuration for circle ci
- A readme
- This changelog
- Heart Renderer

[Unreleased]: https://github.com/rbdr/heart/compare/master...develop
[1.0.1]: https://github.com/rbdr/heart/compare/1.0.0...1.0.1
[1.1.0]: https://github.com/rbdr/heart/compare/1.0.1...1.1.0
[1.1.1]: https://github.com/rbdr/heart/compare/1.1.0...1.1.1
[1.1.2]: https://github.com/rbdr/heart/compare/1.1.1...1.1.2
