# Heart

Renders a colorful heart

## How to run

Serve the files in the root directory. For example using: `python -m SimpleHTTPServer`

## Setting up

Though this project is mostly static you should run `yarn install` or
`npm install` so you can lint and document the project.

## Generating documentation

1. Run `npm run document`.
2. Serve the files that have been generated in the `doc` directory. If
   already serving the files, just go to `/doc`

## Linting

1. Run `npm lint`

## Project Status

![Build Status](https://circleci.com/gh/rbdr/heart.png?circle-token=9ffd4c8b8643b95f6d42047b173e2d446d91b32f)
