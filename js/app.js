'use strict';

// Sets up the application

((window) => {

  const internals = {};

  internals.onLoad = () => {

    const mainElement = window.document.getElementById('heart-app-entry-point');
    const heartRenderer = new HeartRenderer();

    heartRenderer.render(mainElement);
    heartRenderer.activate();

    window.addEventListener('resize', heartRenderer.resize.bind(heartRenderer));
    window.addEventListener('blur', heartRenderer.stopFollowingMouse.bind(heartRenderer));
    window.addEventListener('focus', heartRenderer.startFollowingMouse.bind(heartRenderer));

    /**
     * Exported global object. This will contain the instance of the heart
     * renderer being used. It is set up on load.
     *
     * @name App
     * @type Object
     * @property {HeartRenderer} heartRenderer The instance of the heart renderer being used.
     */
    window.App = {
      heartRenderer
    };
  };

  window.addEventListener('load', internals.onLoad);
})(window);
